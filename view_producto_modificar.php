<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">
     <style>
       form select {
          width: 100%;
          padding: 10px;
          border-radius: 5px;
          border: 2px solid #ccc;
          color: #757575;
        }
     </style>

      <?php include  "cosas-generales/links-generales.php"; ?>
      <link rel="stylesheet" href="css/view_uinsertar_umodificar_usuario.css">

  <title>Tema</title>
</head>
<body>
     <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
           $id_usuario = $_SESSION["usuario"][3];
        }else{
         header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Modificar Tipo de Producto</h1>
    
    <?php 

    	include "php/conexion.php";
    	$conexion = $con;
        $id = $_GET["id"];
        
        $consulta = $conexion->query("SELECT producto.*, tipo_producto.tipo_producto, tipo_producto.id AS mi_id FROM producto
									INNER JOIN tipo_producto ON tipo_producto.id = producto.tipoproducto_id
									INNER JOIN tipo_uso ON tipo_uso.id = tipo_producto.tipouso_id WHERE producto.id = '$id'");

        	// ---- inicio for
			foreach ($consulta as $row) {	?>
			<?php // print_r($row); ?>
				<form action="php/producto/modificar_producto.php" method="post" class="frm-registrarse" id="frm-registrarse">
					<input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>">
          <input type="hidden" class="campo-frm-registrarse" name="id_usuario" value="<?php echo $id_usuario; ?>">
			        <input type="text" class="campo-frm-registrarse" placeholder="Nombre:" name="nombre" value="<?php echo $row['nombre']; ?>">
			        <input type="text" class="campo-frm-registrarse" placeholder="Unidad de medida:" name="unidad_medida" value="<?php echo $row['unidad_medida']; ?>">
			        <!-- <input type="textarea" class="campo-frm-registrarse" placeholder="Nombre" name="descripcion" value="<?php //echo $row['descripcion']; ?>"> -->
			        <input type="number" min="1" class="campo-frm-registrarse" placeholder="Precio:" name="precio" value="<?php echo $row['precio']; ?>">
					
					<?php
                         
                        $consulta = $conexion->query("SELECT * FROM tipo_producto");
							echo '<select name="tipo_producto">';
				            foreach ($consulta as $row2) {

				            		$sel = '';
				            		if($row2['id'] == $row['tipoproducto_id']){
				            			$sel = 'selected';
				            		}
				 
				      				echo $opciones='<option value="'.$row2["id"].'"  '.$sel.'>'.$row2["tipo_producto"].'</option>';
						
				                                                     
				            }              
                                    echo '</select>';	
                      ?>
			        
			        <textarea rows="6" cols=62" name="descripcion" placeholder="Desrcipción del producto" form="frm-registrarse"><?php echo $row["descripcion"]; ?></textarea>



			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">
			        <input type="reset" class="btn-borrar" value="Borrar">
    			</form>
    			
			<!-- /// fin for -->
			<?php } ?>

         <a href="view_producto.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>