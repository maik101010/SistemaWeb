<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

	 <!-- Estilos -->
   <?php include "cosas-generales/links-generales.php"; ?>
   <link rel="stylesheet" href="css/contacto_estilos.css">
    <style>
      body {
        background: #E1F8F2;
      }
    </style>

	<title>Contacto</title>


	 <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
        }else{
            include "cosas-generales/header_usuario.php";
        }
     ?>

   <h1 class="titulo-principal">Información de contacto</h1>
    <div class="contenedor-redes-sociales">
          
        
        <a href="view_contacto.php"><i class="fa fa-envelope-o"></i></a>
        
        <a href="https://www.facebook.com/Global-Agriculture-Consulting-152639728684043/"><i class="fa fa-facebook-official"></i></a>
    </div>
    <main class="contenedor-pagina">
        <div class="contenedor-sidebar">
            <div class="contenedor-item-sidebar">
                <h3>Dirección:</h3>
                <p>Avenidad Ignacio de la Llave No. 182</p>
                <p>Colonia Centro</p>
                <p>C.P 93600</p>
                <p>Martinez de la Torre, Veracruz.</p>
            </div>
            <div class="contenedor-item-sidebar">
                <h3>Teléfono:</h3>
                <p>01 232 373 5240</p>
            </div>
        </div>
    
     <!--    <iframe class="contenedor-mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3736.2320342618186!2d-97.47894588507558!3d20.537684986266402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85da6ad9f25d06db%3A0xca4131fa4aa5110b!2sVeracruz+182%2C+Ignacio+de+la+Llave%2C+93327+Poza+Rica%2C+Ver.%2C+M%C3%A9xico!5e0!3m2!1ses!2sco!4v1526341545259" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
 -->
 <iframe class="contenedor-mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.600913589707!2d-97.05296368554264!3d20.067152186527085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db024b3065a94d%3A0x140df1ea4ddeb016!2sIgnacio+de+La+Llave+182%2C+Centro%2C+Mart%C3%ADnez+de+la+Torre%2C+Ver.%2C+M%C3%A9xico!5e0!3m2!1ses!2sco!4v1526915972776" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </main>
  
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    </head>
<body>
	
</html>