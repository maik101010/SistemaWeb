<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">
     <style>
         .btn-instertar-tema {
            margin-left: 80%;
            margin-bottom: 20px;
         }

         .btn-instertar-tema:hover {
            background: #ddd !important;
            color: #000 !important;
        }

         @media screen and (max-width: 750px) {
            .btn-instertar-tema {
                margin-left: 50%;
             }
         }
     </style>

    <!-- Estilos -->
    <?php include "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_gestionar_tema_estilos.css">

  <title>Tema</title>
</head>
<body>
    <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
        }else{
          header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Gestionar Tipo de uso</h1>

    <a href="view_producto_insertar.php" class="btn btn-outline-dark btn-instertar-tema">Nuevo registro</a>
    
    <?php 
        include "php/conexion.php";

        $conexion = $con;

        $consulta = $conexion->query("SELECT producto.id, producto.precio, producto.nombre, tipo_uso.descripcion, tipo_producto.tipo_producto, producto.fecha_ult_modificacion, usuario.nombres FROM producto
LEFT JOIN tipo_producto ON tipo_producto.id = producto.tipoproducto_id
LEFT  JOIN tipo_uso ON tipo_uso.id = tipo_producto.tipouso_id
LEFT  JOIN usuario ON usuario.id = producto.id_usuario_modificacion");

        ?>
        <!-- Empieza la tabla             -->
        <div class="table-responsive table-hover container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Tipo De Uso</th>
                        <th scope="col">Tipo De Producto</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Última modificación</th>
                        <th scope="col">Modificado por</th>
                        <th scope="col">Operaciones</th>
                        
                    </tr>
                </thead>
            <?php 
            foreach ($consulta as $row) { ?>
            
                    <tbody>
                        <tr>
                            <td><?php echo $row['nombre'] ?></td>
                            <td><?php echo $row['descripcion'] ?></td>
                            <td><?php echo $row['tipo_producto'] ?></td>
                            <td>$<?php echo $row['precio'] ?></td>
                            <td><?php echo $row['fecha_ult_modificacion'] ?></td>
                            <td><?php echo $row['nombres'] ?></td>
                            <td class="contenedor-btn-editar-eliminar">
                                <a href="view_producto_modificar.php?id=<?php echo $row['id'] ?>" class="btn-editar"><i class="fa fa-edit"></i></a>
                                <a href="php/producto/eliminar_producto.php?id=<?php echo $row['id']?>" class="btn-eliminar" onclick="return confirm_delete()"><i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                    </tbody>
                    
            <?php  } ?>
            <!-- Termina la tabla -->
            </table> 
        </div>
          <a href="view_usuario.php" class="btn btn-outline-dark btn-instertar-tema"><i class="fa fa-chevron-left"></i> Regresar</a>
             

    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    <script language="JavaScript">
       function confirm_delete() {
        return confirm('¿Esta usted seguro de eliminar el producto?');
        }
    </script>

  
</body>
</html>