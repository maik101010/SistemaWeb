<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

	<!-- Estilos -->
   	<?php include "cosas-generales/links-generales.php"; ?>
	<link rel="stylesheet" href="css/consultores_estilos.css">
    <style>
      .btn-regresar {
        display: block;
        margin: 20px auto;
        width: 10%;
      }

      @media screen and (max-width: 1000px) {
        .btn-regresar {
          width: 50%;
        }
      }
    </style>

	<title>Consultores</title>
</head>
<body>

    <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
            
        }else{
            include "cosas-generales/header_usuario.php";
            $idUsuario = $_SESSION["usuario"][3];
        }
     ?>

 	<h1 class="titulo-temas">Temas</h1>

		<main class="contenedor-cursos">

		 
		<?php 
		   include "php/conexion.php";
		   $conexion = $con;
		   /*
				Capturamos el valor enviado en el buscador del tema
		   */
		   $valor = $_GET["valor"];
		if (isset($_SESSION["usuario"])) {
			
		
   		if ($_SESSION["usuario"][0]=="Administrador") {

   				
   						$consulta = $conexion->query("SELECT tema.id, tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin, CONCAT(usuario.nombres, '', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id WHERE tema.descripcion
							LIKE '%".$valor."%' AND usuario.estado = 1");
									
							foreach ($consulta as $row) {
								echo "<div class='contenedor-item-curso' >";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info' id='resultado'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Consultor:</span> " . $row['nombre'] . "</p>" . "\t";
										/*
											Enviamor el id del tema para gestionarlo
										*/
										echo "<a href='view_gestionar_tema.php?id=".$row['id']."'><p><span>Gestionar tema</span></p></a>" . "\t";

									echo "</div>";
								echo "</div>";

							}
			}else if ($_SESSION["usuario"][0]=="Consultor") {
				$consulta = $conexion->query("SELECT tema.id, tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin, CONCAT(usuario.nombres, '', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id WHERE tema.descripcion
							LIKE '%".$valor."%' AND usuario.id = '$idUsuario' AND usuario.estado = 1");
									
							foreach ($consulta as $row) {
								echo "<div class='contenedor-item-curso' >";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info' id='resultado'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Consultor:</span> " . $row['nombre'] . "</p>" . "\t";
										/*
											Enviamor el id del tema para gestionarlo
										*/
										echo "<a href='view_gestionar_tema.php?id=".$row['id']."'><p><span>Gestionar tema</span></p></a>" . "\t";

									echo "</div>";
								echo "</div>";

							}
				
			//-----ELSE DEL ISSET DE SESIÓN
			}
		}else{
		    	//----------SESIÓN DE USUARIOS QUE SON CLIENTES
					//include "php/conexion.php";
					$consulta = $conexion->query("SELECT tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin,
					 CONCAT(usuario.nombres, '', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id WHERE tema.descripcion LIKE '%".$valor."%' AND tema.estado = 1");

							foreach ($consulta as $row) {
								echo "<div class='contenedor-item-curso'>";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Instructor:</span> " . $row['nombre'] . "</p>" . "\t";
									echo "</div>";
								echo "</div>";

					}
				}	
			

			?>
		</main>
		<a href="consultores.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
	    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
<!-- 
    <script type="text/javascript">
    	$(document).ready(function(){
		$("#mostrar").on( "click", function() {
			$('#resultado').show(); //muestro mediante id
			//$('.resultado').show(); //muestro mediante clase
		 });
		$("#ocultar").on( "click", function() {
			$('#resultado').hide(); //oculto mediante id
			//$('.resultado').hide(); //muestro mediante clase
		});
	});
    </script> -->
	
</body>
</html>