<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

	<!-- Estilos -->
   	<?php include "cosas-generales/links-generales.php"; ?>
	<link rel="stylesheet" href="css/consultores_estilos.css">
    

	<title>Consultores</title>
</head>
<body>

    <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
        }else{
            include "cosas-generales/header_usuario.php";
        }
     ?>

 	<h1 class="titulo-temas">Temas</h1>

 	   <form action="view_gbuscar_tema.php" method="GET" class="formulario-buscar">
	   		<div class="input-group">
			  	<div class="input-group-append">
			    	<button type="submit"><i class="fa fa-search"></i></button>
			  	</div>
				<input type="search" id="buscar_tema" name="valor" placeholder="Buscar Tema">
			  <a href="view_historial_temas.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-eye"></i>Historial Temas</a>	
			</div>	
	   </form>
	    
		<main class="contenedor-cursos">
		 
		<?php 
		   include "php/conexion.php";
		   $conexion = $con;
		if (isset($_SESSION["usuario"])) {
			
		
   		if ($_SESSION["usuario"][0]=="Administrador") {
   				
   						$consulta = $conexion->query("SELECT tema.id, tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin, tema.costo, CONCAT(usuario.nombres, ' ', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id and usuario.estado = 1 and tema.estado =1");
									
							foreach ($consulta as $row) {
								echo "<div class='contenedor-item-curso' >";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info' id='resultado'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span>Costo:</span> " . "$". $row['costo'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Consultor:</span> " . $row['nombre'] . "</p>" . "\t";
										/*
											Enviamor el id del tema para gestionarlo
										*/
										echo "<a href='view_gestionar_tema.php?id=".$row['id']."'><p><span>Gestionar tema</span></p></a>" . "\t";

									echo "</div>";
								echo "</div>";

							}
			}else if ($_SESSION["usuario"][0]=="Consultor") {
				$contador = 0;

				$sesion = $_SESSION["usuario"];
				$consulta = $conexion->query("SELECT usuario.id, tema.id, tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin, tema.costo, CONCAT(usuario.nombres, ' ', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id where usuario.id='$sesion[3]' and usuario.estado = 1 and tema.estado =1");
							//$result = $con->prepare($sql); 
							$nRows = $consulta->rowCount();
							if ($nRows==0) {
								echo "<a href='view_ginsertar_tema.php' class='btn btn-outline-dark btn-instertar-tema'><i class='fa fa-plus'></i> Nuevo Registro</a>";
							}
							
							//echo "filas " . $nRows;
							
							foreach ($consulta as $row) {

								echo "<div class='contenedor-item-curso' >";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info' id='resultado'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span>Costo:</span> " . "$". $row['costo'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Consultor:</span> " . $row['nombre'] . "</p>" . "\t";
										/*
											Enviamor el id del tema para gestionarlo
										*/
										echo "<a href='view_gestionar_tema.php?id=".$row['id']."'><p><span>Gestionar tema</span></p></a>" . "\t";

									echo "</div>";
								echo "</div>";

							}

							/*if ($nRows==0) {
								echo "<a href='view_ginsertar_tema.php' class='btn btn-outline-dark btn-instertar-tema'><i class='fa fa-plus'></i> Nuevo Registro</a>";
							}*/
				
			}
			
			//-----ELSE DEL ISSET DE SESIÓN
			}else{
		    	//----------SESIÓN DE USUARIOS QUE SON CLIENTES
					//include "php/conexion.php";

					$consulta = $conexion->query("SELECT tema.descripcion, tema.horario, tema.fecha_inicio, tema.fecha_fin, tema.costo,
					 CONCAT(usuario.nombres, '', usuario.a_paterno) AS nombre
							FROM usuario INNER JOIN tipo_usuario ON tipo_usuario.id = usuario.tipo
							INNER JOIN tema ON tema.usuario_id = usuario.id and usuario.estado = 1 and tema.estado =1");

							foreach ($consulta as $row) {
								echo "<div class='contenedor-item-curso'>";
									echo "<div class='contenedor-img'>";
										echo "<img src='img/cursos.png' alt='Imágen de los Cursos'>";
									echo "</div>";
									echo "<div class='contenedor-info'>";
										echo "<p><span>Descripción:</span> " . $row['descripcion'] . "</p>" . "\t";
										echo "<p><span>Horario:</span> " . $row['horario'] . "</p>" . "\t";
										echo "<p><span>Fecha de Inicio:</span> " . $row['fecha_inicio'] . "</p>" . "\t";
										echo "<p><span>Fecha de Fin:</span> " . $row['fecha_fin'] . "</p>" . "\t";
										echo "<p><span>Costo:</span> " . "$". $row['costo'] . "</p>" . "\t";
										echo "<p><span class='p-nombre-instructor'>Nombre del Instructor:</span> " . $row['nombre'] . "</p>" . "\t";
									echo "</div>";
								echo "</div>";

					}
				}	

			?>
		</main>
	    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

</body>
</html>