<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php 
    /*
		para el manejo de rutas al futuro
    */
    	//require_once "php/config.php";

    	  
        
        
     ?>

    <?php include  "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_ginsertar_tema_estilos.css">
    <style>
      .btn-regresar {
        display: block;
        margin: 20px auto;
        width: 10%;
      }

      @media screen and (max-width: 1000px) {
        .btn-regresar {
          width: 50%;
        }
      }
    </style>

  <title>Tema</title>
</head>
<body>
    <?php
        session_start();


        if ($_SESSION["usuario"][0]=="Administrador" || $_SESSION["usuario"][0]=="Consultor") {
           include "cosas-generales/header_usuario.php";
           $usuarioId = $_SESSION["usuario"][3];
        }else{
          header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Insertar Tema</h1>

	<form action="php/tema/insertar_tema.php" method="post" class="frm-registrarse" id="frm-registrarse">
		
				
		    <input type="text" class="campo-frm-registrarse" name="horario" placeholder="Horario:">
		    <input type="text" class="campo-frm-registrarse" name="direccion" placeholder="Direccion:">
		    <input type="date" class="campo-frm-registrarse" name="fecha_inicio" placeholder="fecha de inicio:"> 
		    <input type="date" class="campo-frm-registrarse" name="fecha_fin" placeholder="fecha de cierre:">
		    <input type="number" class="campo-frm-registrarse" name="costo" placeholder="Costo:">
                  <select name="usuario_id">
                  <option value="0">Elige un consultor</option>
                  <?php
                          include "php/conexion.php";
                          $conexion = $con;
                          if ($_SESSION["usuario"][0]=="Administrador") {
                          
                          $consulta = $conexion->query("SELECT tipo_usuario.tipo, usuario.id, usuario.nombres, usuario.estado FROM usuario INNER JOIN tipo_usuario
                                ON usuario.tipo = tipo_usuario.id WHERE tipo_usuario.id = 3 AND usuario.estado = 1");

                                foreach ($consulta as $row) {
                                         $opciones='<option value="'.$row["id"].'">'.$row["nombres"].'</option>';
                                         echo $opciones;
                                }   
                          }else if ($_SESSION["usuario"][0]=="Consultor") {
                            $consulta = $conexion->query("SELECT tipo_usuario.tipo, usuario.id, usuario.nombres, usuario.estado FROM usuario INNER JOIN tipo_usuario
                                ON usuario.tipo = tipo_usuario.id 
                                WHERE tipo_usuario.id = 3 AND usuario.estado = 1 and usuario.id = '$usuarioId'");

                                foreach ($consulta as $row) {
                                         $opciones='<option value="'.$row["id"].'">'.$row["nombres"].'</option>';
                                         echo $opciones;
                                }   
                          }


                      ?>
                
                    
                       
                          
                    </select>

			<input type="text" class="campo-frm-registrarse" name="descripcion" placeholder="descripcion"> 
		        
		    <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Insertar tema">
		    <input type="reset" class="btn-borrar" value="Borrar">
	
	</form>
   
    <!-- 
 -->
 <a href="view_gestionar_tema.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
   
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

</body>
</html>