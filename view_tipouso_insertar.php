<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">



    <?php include  "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_ginsertar_tema_estilos.css">

  <title>Tema</title>
</head>
<body>
       <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
        }else{
          header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Insertar Tipo de uso productos</h1>

	<form action="php/tipo_uso/insertar_tipo_uso.php" method="post" class="frm-registrarse" id="frm-registrarse">
		
				
		    <input type="text" class="campo-frm-registrarse" name="descripcion" placeholder="Descripción:">
		        
		    <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Insertar">
		    <input type="reset" class="btn-borrar" value="Borrar">
	
	</form>
   
    <!-- 
 -->
     <a href="view_tipouso_productos.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    <script type="text/javascript">
              
     

    </script>
  
</body>
</html>