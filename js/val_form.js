(function(){

	formulario = document.getElementById("frm-registrarse");

	var validarInputs = function(e){
		var valores =formulario.getElementsByTagName("input");
		for (var i=0; i<valores.length; i++)
			
			if (valores[i].value == 0){
				 alert("Falta llenar el campo "+ valores[i].name);
				 e.preventDefault()

			}/**
			 * Empieza la validación de los campos númericos
			 * @param  valores[i].name [los valores recorridos del array del formulario]
			 */
			else if (valores[i].name=="telefono" || valores[i].name=="costo" || valores[i].name=="precio" || valores[i].name=="celular") {
				var campo=valores[i].value
				if (!/^([0-9])*$/.test(campo)){				
					alert("El valor " + campo + " no es númerico")
					e.preventDefault()
				}
			}
			/**
			 * Empieza la validación de los campos de texto
			 * @param  valores[i].name [los valores recorridos del array del formulario]
			 */
			else if (valores[i].name=="nombre" || valores[i].name=="nombres" || valores[i].name=="a_paterno" || valores[i].name=="a_materno" || valores[i].name=="ciudad" || valores[i].name=="pais" || valores[i].name=="unidad_medida" || valores[i].name=="direccion" || valores[i].name=="descripcion" || valores[i].name=="tipo_producto") {
				var filtro = /[a-zA-Z]/
				var campo = valores[i].value
				if (!filtro.test(campo)){
					alert("El valor " + campo + " no es texto ")
					e.preventDefault()
				}
			}

		};

	var validarSelects = function(e){
		var valores = formulario.getElementsByTagName("select");
		for (var i=0; i<valores.length; i++)
		if (valores[i].value == 0){
			 alert("Falta Seleccionar");
			 e.preventDefault()

		}

	}
	var validarContrasenia = function(e){
		var contrasenia = document.getElementById("contra").value

    	var contraseniaRepeat = document.getElementById("contraseniaRepeat").value

    	if (contraseniaRepeat!= contrasenia) {
    		alert("Las contraseñas no coinciden");
    		e.preventDefault()
    	}

	}

		var validar = function(e){
			validarInputs(e);
			validarSelects(e);			

			//validarContrasenia(e);
		};


	formulario.addEventListener("submit", validar);

}())