SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE IF NOT EXISTS db_global;

USE db_global;

DROP TABLE IF EXISTS producto;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` double DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `unidad_medida` varchar(40) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tipoproducto_id` int(11) DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipoproducto_id` (`tipoproducto_id`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`tipoproducto_id`) REFERENCES `tipo_producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO producto VALUES("3","790","calgonit 6010","bidon 20 kg","","3","calgonit  6010 es un detergente alcalino altamente concentrado. No crea espuma y es fácilmente controlado por conductividad y está recomendado para la limpieza por recirculación.","10","2019-01-21");
INSERT INTO producto VALUES("4","600","Calgonit 6020","bidon 30 kg","","3","Calgonit 6020, Aditivo sin cloro, No crea espuma y es fácilmente controlado por conductividad y está recomendado para la limpieza por  pulverización de depósitos, pasteurizadores, tuberías, calentadores.","10","2019-01-21");
INSERT INTO producto VALUES("5","460","Calgonit 6021","bidon 20 kg","","3","Calgonit 6021, está recomendado para la limpieza por recirculación y pulverización de depósitos, pasteurizadores, tuberías, calentadores.","10","2019-01-21");
INSERT INTO producto VALUES("6","850","Calgonit sterizid forte 15","40 kg","","4","Calgonit sterizid forte 15, un desinfectante muy usual en las industrias cafetaleras, para limpieza de sus recipientes de almacenamiento.","10","2019-01-21");
INSERT INTO producto VALUES("7","300","Calgonit sporex","1 kg","","5","Calgonit sporex, este es un espumante alcalino, recomendado para una mayor limpieza superficial, ocupado en distintas areas de limpieza en Industrias Mantequeras.","10","2019-01-21");
INSERT INTO producto VALUES("8","400","Calgonit CF314","20 kg","","5","Calgonit CF314, este es un espumante alcalino, recomendado para una mayor limpieza superficial, ocupado en distintas areas de limpieza en Industrias Mantequeras.","10","2019-01-21");
INSERT INTO producto VALUES("9","960","Calgonit NW5454","30 kg","","6","Calgonit NW5454, excelente detergente para industrias lecheras, lo cual limpia y desinfecta tuberias de alta precion y calderas.","10","2019-01-21");
INSERT INTO producto VALUES("10","550","Calgonit LAO","20 kg","","7","Calgonit LAO, producto altamente espumoso utilizado en industrias donde no se puede llegar a desinfectar con suficiente liquido.","10","2019-01-21");



DROP TABLE IF EXISTS tema;

CREATE TABLE `tema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horario` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  CONSTRAINT `tema_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO tema VALUES("2","1pm-4pm","Martinez de la torre","2019-03-01","2019-02-08","2000","10","Medio Ambiente","2019-02-15","9","1");
INSERT INTO tema VALUES("4","9am-12pm","Martinez de la torre","2019-01-20","2019-01-27","400","9","Productos naturales","2019-01-19","17","1");
INSERT INTO tema VALUES("7","12pm- 4pm","Martinez de la torre","2019-02-01","2019-02-08","900","9","Agricultura","2019-01-19","17","1");
INSERT INTO tema VALUES("14","2pm-5pm","Martinez de la torre","2019-02-15","2019-02-22","1200","9","Medicion de PH","2019-01-19","17","1");
INSERT INTO tema VALUES("15","12pm- 4pm","Martinez de la torre","2019-02-15","2019-02-22","1800","10","Agricultura","2019-01-20","17","1");
INSERT INTO tema VALUES("17","9am-12pm","Martinez de la torre","2019-01-19","2019-01-20","690","18","Industria lactea","2019-02-26","18","1");



DROP TABLE IF EXISTS tipo_producto;

CREATE TABLE `tipo_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_producto` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tipouso_id` int(11) DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipouso_id` (`tipouso_id`),
  CONSTRAINT `tipo_producto_ibfk_1` FOREIGN KEY (`tipouso_id`) REFERENCES `tipo_uso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO tipo_producto VALUES("3","Detergentes Alcalinos","1","10","2019-01-21");
INSERT INTO tipo_producto VALUES("4","Desinfeccion","3","10","2019-01-21");
INSERT INTO tipo_producto VALUES("5","Alcalinos Espumantes","2","10","2019-01-21");
INSERT INTO tipo_producto VALUES("6","Detergentes base agua","1","10","2019-01-21");
INSERT INTO tipo_producto VALUES("7","Acidos Espumantes","1","10","2019-01-21");



DROP TABLE IF EXISTS tipo_uso;

CREATE TABLE `tipo_uso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO tipo_uso VALUES("1","Industria lactea","10","2019-01-21");
INSERT INTO tipo_uso VALUES("2","Instalaciones Mantequeras","10","2019-01-21");
INSERT INTO tipo_uso VALUES("3","Industria Cafetera","10","2019-01-21");



DROP TABLE IF EXISTS tipo_usuario;

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO tipo_usuario VALUES("1","Administrador");
INSERT INTO tipo_usuario VALUES("3","Consultor");



DROP TABLE IF EXISTS usuario;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `a_paterno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `a_materno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `contrasenia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `pais` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `ciudad` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `tipo` (`tipo`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

INSERT INTO usuario VALUES("9","Omar ","Florez","Ramirez","omar@gmail.com","$2y$12$C4UsZfrhenqM/jdGp8nVa.4.0KLfyd5RPVt218PSMj4wOmaX9r8tC","2310565","2321279832","Mexico a","Martinez de la Torre","3","1","9","2019-02-20");
INSERT INTO usuario VALUES("10","Mario Alberto","Perez","Ramos","mario.13yanet@gmail.com","$2y$12$SXcXXUvG2.JBJNY0dpysg.8Q5j8hOJVe7XiqfCDVhBqP2e.XL1IsK","2323731201","2321090909","Mexico","Martinez de la Torre","1","1","17","2019-01-19");
INSERT INTO usuario VALUES("15","Samuel","Arellanes","Martinez","samuel@gmail.com","$2y$12$7BbmHONQ6Uc7HXK5MzqUQ.idHEEzaXw2Fc7qRDR9SpHVZnkcSojD.","321654789","2321030314","Mexico","Martinez de la Torre","3","1","10","2019-01-21");
INSERT INTO usuario VALUES("17","Iram","Tenorio","Abelló","iram96@gmail.com","$2y$12$iD94KwQ/5zJFg0.1Omh10OhAe3K0Q7D8MisCipFNMYRzAKV5Y1LrC","56423154","2321212314","Mexico","Martinez de la Torre","3","1","10","2019-01-21");
INSERT INTO usuario VALUES("18","Norma","Galindo","Suarez","norma@gmail.com","$2y$12$qs462DN8wR3zBysJA6LdzOtPQpty3tiApiDibULwimjNrZ8dw.2Ja","51236484","2321030312","Mexico","Martinez de la Torre","3","1","10","2019-01-21");
INSERT INTO usuario VALUES("19","Alex","Florez","Gomez","alex@gmail.com","$2y$12$6WGc4ncY9rn3RRCUPK.chuhviqNB55Z.L9vUQ1/ZmeqGOysOkLw7C","2325654589","23156458","Mexico","Martinez de la Torre","3","1","19","2019-02-20");
INSERT INTO usuario VALUES("20","Maria","Perez","Romero","maria@gmail.com","$2y$12$Oh9.BLo.coM088bzOeLsyeiYwi1hX8hM5MHcGfDhjyndt9Y7daRC2","12221","333222111","Mexico","Martinez de la Torre","3","1","10","2019-01-21");



SET FOREIGN_KEY_CHECKS=1;