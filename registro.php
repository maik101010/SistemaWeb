<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php include "cosas-generales/links-generales.php"; ?>

  <title>Registrarse</title>
</head>
<body>

      <?php include "cosas-generales/header.php";?>

      <form action="php/registrar_usuario.php" method="post" class="frm-registrarse" id="frm-registrarse">
            <input type="text" class="campo-frm-registrarse" name="nombres" placeholder="Nombres: ">
            <input type="text" class="campo-frm-registrarse" name="aPaterno" placeholder="Apellido paterno: ">
            <input type="text" class="campo-frm-registrarse" name="aMaterno" placeholder="Apellido materno: ">
            <input type="text" class="campo-frm-registrarse" name="telefono" placeholder="Teléfono: ">
            <input type="text" class="campo-frm-registrarse" name="cel" placeholder="Teléfono Celular: ">
            <input type="text" class="campo-frm-registrarse" name="pais" placeholder="Pais: ">
            <input type="text" class="campo-frm-registrarse" name="ciudad" placeholder="Ciudad: ">
            <input type="email" class="campo-frm-registrarse" name="email" placeholder="Email: ">
            <input type="password" class="campo-frm-registrarse" name="contraseña" placeholder="Contraseña: ">
            <input type="password" class="campo-frm-registrarse" name="repetir-contraseña" placeholder="Repetir Contraseña: ">

            <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Registrarse">
            <input type="reset" class="btn-borrar" value="Borrar">
      </form>
  
</body>
</html>