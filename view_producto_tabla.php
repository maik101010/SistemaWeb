<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

	<!-- Estilos -->
   	<?php include "cosas-generales/links-generales.php"; ?>
	<link rel="stylesheet" href="css/productos_estilos.css">
    

	<title>Productos</title>
</head>
<body>

	<?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        
        session_start();
         if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
        }else{
            include "cosas-generales/header_usuario.php";
        }
    ?>
    <!-- Slider -->
        <script src="js/jssor.slider.js" type="text/javascript"></script>
        <script type="text/javascript">
            jssor_1_slider_init = function() {

                var jssor_1_SlideoTransitions = [
                  [{b:900,d:2000,x:-379,e:{x:7}}],
                  [{b:900,d:2000,x:-379,e:{x:7}}],
                  [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
                ];

                var jssor_1_options = {
                  $AutoPlay: 1,
                  $SlideDuration: 800,
                  $SlideEasing: $Jease$.$OutQuint,
                  $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions
                  },
                  $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                  },
                  $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                  }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive code begin*/
                /*remove responsive code if you don't want the slider scales while window resizing*/
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 1920);
                        jssor_1_slider.$ScaleWidth(refSize);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*responsive code end*/
            };
        </script>
        <style>
            /* jssor slider bullet navigator skin 05 css */
            /*
            .jssorb05 div           (normal)
            .jssorb05 div:hover     (normal mouseover)
            .jssorb05 .av           (active)
            .jssorb05 .av:hover     (active mouseover)
            .jssorb05 .dn           (mousedown)
            */
            .jssorb05 {
                position: absolute;
            }
            .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                position: absolute;
                /* size of bullet elment */
                width: 16px;
                height: 16px;
                background: url('img/controles/b05.png') no-repeat;
                overflow: hidden;
                cursor: pointer;
            }
            .jssorb05 div { background-position: -7px -7px; }
            .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
            .jssorb05 .av { background-position: -67px -7px; }
            .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
            .jssora22l, .jssora22r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 58px;
                cursor: pointer;
                background: url('img/controles/a22.png') center center no-repeat;
                overflow: hidden;
            }
            .jssora22l { background-position: -10px -31px; }
            .jssora22r { background-position: -70px -31px; }
            .jssora22l:hover { background-position: -130px -31px; }
            .jssora22r:hover { background-position: -190px -31px; }
            .jssora22l.jssora22ldn { background-position: -250px -31px; }
            .jssora22r.jssora22rdn { background-position: -310px -31px; }
            .jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
            .jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }
        </style>
        <div id="jssor_1" style="position:relative;margin:0 auto 60px auto;top:55px;left:0px;width:1300px;height:300px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('img/controles/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:300px;overflow:hidden;">
                <!--IMAGENES DE SLIDER-->
                <div>
                    <img data-u="image" src="img/banners/13.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/13.png">
                </div>
                
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- Slider -->



    


    <h1 class="titulo-productos">Productos</h1>
    
    <?php 
			include "php/conexion.php";

			$conexion = $con;
			$consulta = $conexion->query("SELECT * FROM tipo_uso");


    ?>
    
    <?php 
  include "php/conexion.php";

  $conexion = $con;
  //$id_tipo = $_POST["id_tipo_producto"];
  $consulta = $conexion->query("SELECT producto.unidad_medida,producto.nombre, producto.descripcion, tipo_producto.tipo_producto,
tipo_uso.descripcion,  producto.precio
from producto inner join tipo_producto on tipo_producto.id = producto.tipoproducto_id
inner join tipo_uso on tipo_uso.id = tipo_producto.tipouso_id
");
?>
    <div class="row contenedor-tabla-busqueda">
      <div class="contenedor-busqueda col-md-4">
       <input type="text" id="buscar" placeholder="Buscar Por Nombre:">
       <div id="resultado"></div>
      </div>

      <div class="table-responsive col-md-8">
        <table class="table table-hover">
              <thead>
                  <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Unidad medida</th>
                      <th scope="col">Precio</th>
                      <!-- <th scope="col">Operaciones</th> -->
                  </tr>
              </thead>
              
          <?php 

          foreach ($consulta as $row) { ?>
          
              <tbody>
                  <tr>
                      <!-- <td><?php //echo $row['id'] ?></td> -->
                      <td><?php echo $row['nombre'] ?></td>
                      <td><?php echo $row['unidad_medida'] ?></td>
                      <td><p>$<?php echo $row['precio'] ?></p></td>
                     
                      <!-- <td> -->
                         <!-- <a href="mostrar_pdf.php?id=<?php //echo $row['id'] ?>" target="_blank"><i class="fa fa-eye"></i></a>-->
                          
                      <!-- </td> -->
                  </tr>
              </tbody>
                  
               <?php  } ?>
          </table>
      </div>      
    </div>
    


    <?php include "cosas-generales/scripts-generales.php"; ?>

    <?php include "cosas-generales/footer.php"; ?>
    
    <script type="text/javascript">
      $(document).ready(function(e){
          $("#buscar").keyup(function(e){

            var nombre = $("#buscar").val();
            /*
              Capturamos el id del tipo de producto
            */
            //var id = <?php// echo $id_tipo ?>;
            $.ajax({
              type: "post",
              url: "php/producto/buscar_producto.php",
              dataType:"html",
              /*
              Enviamos el id del tipo de producto, junto al nombre digitado en el campo de texto
              */  
              data:{nombre : nombre}, 
              success: function(dato){
                $("#resultado").empty();
                $("#resultado").append(dato);

              }
            });
          });

      });
	
    </script>
	
</body>

</html>