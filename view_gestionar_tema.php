<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php include "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_gestionar_tema_estilos.css">

  <title>Tema</title>
</head>
<body>
    <?php
       session_start();
      if (!isset($_SESSION["usuario"])) {
            header("Location: index.php");    # code...
       }else if ($_SESSION["usuario"][0]=="Administrador" || $_SESSION["usuario"][0]=="Consultor") {

           include "cosas-generales/header_usuario.php";
           //Capturamos el id del usuario
           $id_usuario_sesion = $_SESSION["usuario"][3];
        }else{
          header("Location : index.php");
        }
    ?>

    <h1 class="titulo-principal">Gestionar Temas</h1>

    <div class="contenedor-botones-gestionar-usuarios">
        <a href="consultores.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
        <a href="view_ginsertar_tema.php" class="btn btn-outline-dark btn-instertar-tema"><i class="fa fa-plus"></i> Nuevo Registro</a>
    </div>
             
    
    <?php 
        include "php/conexion.php";

        if ($_SESSION["usuario"][0]=="Administrador") {
              $conexion = $con;
        //Actualizada, Consulta para traer la fecha inicio y fecha fin y ult modificación en el sistema
        $consulta = $conexion->query("SELECT tema.estado, tema.fecha_ult_modificacion, tema.id, tema.horario, tema.direccion, tema.fecha_inicio, tema.fecha_fin, tema.costo, tema.descripcion, CONCAT(usuario.nombres, ' ', usuario.a_paterno) as nombre_per FROM tema inner JOIN usuario ON usuario.id = tema.usuario_id and usuario.estado = 1");
        ?>
        <!-- Empieza la tabla             -->
        <div class="table-responsive table-hover container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Horario</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Fecha inicio</th>
                        <th scope="col">Fecha final</th>
                        <th scope="col">Costo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Consultor</th>
                        <th scope="col">Última modificación</th>
                        <!-- <th scope="col">Modificado por</th> -->
                        <th scope="col">Estado</th>
                        <th scope="col">Operaciones</th>
                    </tr>
                </thead>
            <?php 
            foreach ($consulta as $row) { ?>
            
                    <tbody>
                        <tr>
                            <td><?php echo $row['horario'] ?></td>
                            <td><?php echo $row['direccion'] ?></td>
                            <td><?php echo $row['fecha_inicio'] ?></td>
                            <td><?php echo $row['fecha_fin'] ?></td>
                            <td>$<?php echo $row['costo'] ?></td>
                            <td><?php echo $row['descripcion'] ?></td>
                            <td><?php echo $row['nombre_per'] ?></td>
                            <td><?php echo $row['fecha_ult_modificacion'] ?></td>
                            <!-- <td><?php //echo $usuario_modificacion ?></td> -->

                            <?php if ($row['estado']=='1') {
                                echo "<td>Activo</td>";
                            } else
                                echo "<td>Inactivo</td>";
                            ?>
                            <td class="contenedor-btn-editar-eliminar">
                                <?php if ($row['estado']=='1') {?>
                                <a href="view_gmodificar_tema.php?id=<?php echo $row['id'] ?>" class="btn-editar"><i class="fa fa-edit"></i></a>
                                <a href="php/tema/eliminar_tema.php?id=<?php echo $row['id']?>&usuario_modificacion=<?php echo $id_usuario_sesion; ?>" class="btn-eliminar" onclick="return confirm_delete()"><i class="fa fa-trash"></i></a>
                            <?php } ?>
                            </td>
                        </tr>
                    </tbody>
                    
            <?php  }
                
             ?>
            <!-- Termina la tabla y el if -->
            </table> 
        </div>
        <?php 
        //Termina el if de administrador
        }else if ($_SESSION["usuario"][0]=="Consultor") {

        $conexion = $con;
        //Actualizada, Consulta para traer la fecha inicio y fecha fin y ult modificación en el sistema
        $consulta = $conexion->query("SELECT usuario.id, tema.estado, tema.fecha_ult_modificacion, tema.id, tema.horario, tema.direccion, tema.fecha_inicio, tema.fecha_fin, tema.costo, tema.descripcion, CONCAT(usuario.nombres, ' ', usuario.a_paterno) as nombre_per FROM tema inner JOIN usuario ON usuario.id = tema.usuario_id where usuario.id = '$id_usuario_sesion' and usuario.estado = 1");
        ?>
        <!-- Empieza la tabla             -->
        <div class="table-responsive table-hover container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Horario</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Fecha inicio</th>
                        <th scope="col">Fecha final</th>
                        <th scope="col">Costo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Consultor</th>
                        <th scope="col">Última modificación</th>
                        <!-- <th scope="col">Modificado por</th> -->
                        <th scope="col">Estado</th>
                        <th scope="col">Operaciones</th>
                    </tr>
                </thead>
            <?php 
            foreach ($consulta as $row) { ?>
            
                    <tbody>
                        <tr>
                            <td><?php echo $row['horario'] ?></td>
                            <td><?php echo $row['direccion'] ?></td>
                            <td><?php echo $row['fecha_inicio'] ?></td>
                            <td><?php echo $row['fecha_fin'] ?></td>
                            <td>$<?php echo $row['costo'] ?></td>
                            <td><?php echo $row['descripcion'] ?></td>
                            <td><?php echo $row['nombre_per'] ?></td>
                            <td><?php echo $row['fecha_ult_modificacion'] ?></td>
                            <!-- <td><?php //echo $usuario_modificacion ?></td> -->

                            <?php if ($row['estado']=='1') {
                                echo "<td>Activo</td>";
                            } else
                                echo "<td>Inactivo</td>";
                            ?>
                            <td class="contenedor-btn-editar-eliminar">
                                <?php if ($row['estado']=='1') {?>
                                <a href="view_gmodificar_tema.php?id=<?php echo $row['id'] ?>" class="btn-editar"><i class="fa fa-edit"></i></a>
                                <a href="php/tema/eliminar_tema.php?id=<?php echo $row['id']?>&usuario_modificacion=<?php echo $id_usuario_sesion; ?>" class="btn-eliminar" onclick="return confirm_delete()"><i class="fa fa-trash"></i></a>
                            <?php } ?>
                            </td>
                        </tr>
                    </tbody>
                    
            <?php  }
                }
             ?>
            <!-- Termina la tabla y el if -->
            </table> 
        </div>
    
    

    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    <script language="JavaScript">
       function confirm_delete() {
        return confirm('¿Esta usted seguro?');
        }
    </script>
  
</body>
</html>