<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <!-- Fuentes De Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <?php include "../../cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_gestionar_tema_estilos.css">

  <title>Inicio</title>

<style type="text/css">
	
/*
html{

	height: 100%;
	width: 100%;
}

body{

background: #8ba987 url('logo1.png')no-repeat center center;
background-size: 100% 100%;



}*/

</style>
</head>
<body>

	   <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "../../cosas-generales/header.php";
        }else{
            include "../../cosas-generales/header_usuario.php";
        }
     ?>
<BR>
<BR>

<center><p><h1>  RESPALDO BASE DE DATOS </h1></p></center>

<BR>
<BR>


	<center><p><h1><a href="./Backup.php">Realizar copia de seguridad</a></h1></p></center>
	<form action="./Restore.php" method="POST">
		<center><h2><label>Selecciona un punto de restauración</label><br></h2></center>


		<div style="text-align: center;">
		<select name="restorePoint">
			<option value="" disabled="" selected="">Selecciona un punto de restauración</option>
			<?php
				include_once './Connet.php';
				$ruta=BACKUP_PATH;
				if(is_dir($ruta)){
				    if($aux=opendir($ruta)){
				        while(($archivo = readdir($aux)) !== false){
				            if($archivo!="."&&$archivo!=".."){
				                $nombrearchivo=str_replace(".sql", "", $archivo);
				                $nombrearchivo=str_replace("-", ":", $nombrearchivo);
				                $ruta_completa=$ruta.$archivo;
				                if(is_dir($ruta_completa)){
				                }else{
				                    echo '<option value="'.$ruta_completa.'">'.$nombrearchivo.'</option>';
				                }
				            }
				        }
				        closedir($aux);
				    }
				}else{
				    echo $ruta." No es ruta válida";
				}
			?>
		</select>

</div>
<br>
<br>


	<center>	<button type="submit"  style='width:70px; height: 25px;background: #F0FCA0'>Restaurar</button></center>
	<BR>

	<CENTER>
		<input type='button' value='Atras' name="boton" style='width: 70px;height: 25px;background: #F0FCA0;' onclick='history.go(-1);'>

	</CENTER>
	</form>
	<?php include "../../cosas-generales/footer.php"; ?>

    <?php include "../../cosas-generales/scripts-generales.php"; ?>
</body>
</html>
