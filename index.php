<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <!-- Fuentes De Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

  <!-- Estilos -->
  <?php include "cosas-generales/links-generales.php"; ?>
  <link rel="stylesheet" href="css/index-estilos.css">

  <title>Inicio</title>
</head>
<body>

   
    <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
        }else{
            include "cosas-generales/header_usuario.php";
        }
     ?>
   
   <?php// if ($_SESSION["validacion"]== "admin" || $_SESSION["validacion"]=="cliente" || $_SESSION["validacion"]=="consultor"): ?>
               <?php //include "cosas-generales/header_usuario.php"; ?>
         <?php //else: ?>
               <?php //include "cosas-generales/header.php";?> 
                        
   <?php //endif ?>
    <!-- Slider -->
        <script src="js/jssor.slider.js" type="text/javascript"></script>
        <script type="text/javascript">
            jssor_1_slider_init = function() {

                var jssor_1_SlideoTransitions = [
                  [{b:900,d:2000,x:-379,e:{x:7}}],
                  [{b:900,d:2000,x:-379,e:{x:7}}],
                  [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
                ];

                var jssor_1_options = {
                  $AutoPlay: 1,
                  $SlideDuration: 800,
                  $SlideEasing: $Jease$.$OutQuint,
                  $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions
                  },
                  $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                  },
                  $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                  }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive code begin*/
                /*remove responsive code if you don't want the slider scales while window resizing*/
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 1920);
                        jssor_1_slider.$ScaleWidth(refSize);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*responsive code end*/
            };
        </script>
        <style>
            /* jssor slider bullet navigator skin 05 css */
            /*
            .jssorb05 div           (normal)
            .jssorb05 div:hover     (normal mouseover)
            .jssorb05 .av           (active)
            .jssorb05 .av:hover     (active mouseover)
            .jssorb05 .dn           (mousedown)
            */
            .jssorb05 {
                position: absolute;
            }
            .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                position: absolute;
                /* size of bullet elment */
                width: 16px;
                height: 16px;
                background: url('img/controles/b05.png') no-repeat;
                overflow: hidden;
                cursor: pointer;
            }
            .jssorb05 div { background-position: -7px -7px; }
            .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
            .jssorb05 .av { background-position: -67px -7px; }
            .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

            /* jssor slider arrow navigator skin 22 css */
            /*
            .jssora22l                  (normal)
            .jssora22r                  (normal)
            .jssora22l:hover            (normal mouseover)
            .jssora22r:hover            (normal mouseover)
            .jssora22l.jssora22ldn      (mousedown)
            .jssora22r.jssora22rdn      (mousedown)
            .jssora22l.jssora22lds      (disabled)
            .jssora22r.jssora22rds      (disabled)
            */
            .jssora22l, .jssora22r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 58px;
                cursor: pointer;
                background: url('img/controles/a22.png') center center no-repeat;
                overflow: hidden;
            }
            .jssora22l { background-position: -10px -31px; }
            .jssora22r { background-position: -70px -31px; }
            .jssora22l:hover { background-position: -130px -31px; }
            .jssora22r:hover { background-position: -190px -31px; }
            .jssora22l.jssora22ldn { background-position: -250px -31px; }
            .jssora22r.jssora22rdn { background-position: -310px -31px; }
            .jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
            .jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }
        </style>
        <div id="jssor_1" style="position:relative;margin:0 auto 60px auto;top:55px;left:0px;width:1300px;height:300px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('img/controles/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:300px;overflow:hidden;">
                <!--IMAGENES DE SLIDER-->
                <div>
                    <img data-u="image" src="img/banners/1.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/2.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/3.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/4.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/5.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/6.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/7.jpg">
                </div>
                <div>
                    <img data-u="image" src="img/banners/8.jpg">
                </div>
                <div>
                    <img data-u="image" src="img/banners/9.jpg">
                </div>
                <div>
                    <img data-u="image" src="img/banners/10.jpg">
                </div>
                <div>
                    <img data-u="image" src="img/banners/11.jpg">
                </div>
                <div>
                    <img data-u="image" src="img/banners/12.png">
                </div>
                <div>
                    <img data-u="image" src="img/banners/13.png">
                </div>
               
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- Slider -->

    <h1 class="titulo-principal">Inicio</h1>

    <main class="contenedor-pagina">
        <div class="contenedor-servicios">
            <div class="contenedor-items-servicios">
                <article>
                    <a href="#" id="img-auditoria"><img src="img/auditoria.png" alt="Imágen"></a>
                    <div class="titular-articulo">
                        <h4>Auditoría</h4>
                    </div>
                    <p id="texto-auditoria">Las auditorías de calidad ofrecen a las organizaciones confianza sobre la eficacia de su sistema de gestión de la calidad y su capacidad para cumplir los requisitos del cliente. Igualmente, las organizaciones pueden acceder a la obtención de certificados de gestión de la calidad a través de un proceso de auditoría de calidad que lleva a cabo una entidad certificadora.</p>
                </article>

<img src="img/calendario.png.gif" onmouseover="this.width=500;this.height=500;" onmouseout="this.width=400;this.height=400;" width="300" height="300" alt="Imágen" />
               <!--  <article>
                    <a href="#" id="img-calendario">
                        <img src="img/calendario.png" alt="Imágen"> 

                    </a>
                    <div class="titular-articulo">
                        <h4>Calendario</h4>
                    </div>
                    
                    <p id="texto-calendario">“El tiempo es la divisa de tu vida. Es la única divisa que posees y solo tú puedes determinar cómo debe ser gastada. Sé cuidadoso y no permitas que otras personas la gasten por ti.”  (Carl Sandburg).</p>
                </article> -->

                <article>
                    <a href="#" id="img-consultoria"><img src="img/consultoria.png" alt="Imágen"></a>
                    <div class="titular-articulo">
                        <h4>Consultoría</h4>
                    </div>

                    <div id="carouselConsultoriaIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselConsultoriaIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselConsultoriaIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselConsultoriaIndicators" data-slide-to="2"></li>
                            <li data-target="#carouselConsultoriaIndicators" data-slide-to="3"></li>
                            <li data-target="#carouselConsultoriaIndicators" data-slide-to="4"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img class="d-block w-100" src="img/slider-consultoria/img1.png" alt="First slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100" src="img/slider-consultoria/img2.png" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100" src="img/slider-consultoria/img3.png" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100" src="img/slider-consultoria/img4.png" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100" src="img/slider-consultoria/img5.png" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselConsultoriaIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselConsultoriaIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </article>
            </div>
        </div>
    </main>

  

    
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
    <script>
        $(document).ready(function(){
            $("#texto-auditoria").hide();
            $("#img-auditoria").click(function(e) {
                e.preventDefault();
                $("#texto-auditoria").toggle("fast");
            });

            $("#texto-calendario").hide();
            $("#img-calendario").click(function(e) {
                e.preventDefault();
                $("#texto-calendario").toggle("fast");
            });

            $("#carouselConsultoriaIndicators").hide();
            $("#img-consultoria").click(function(e) {
                e.preventDefault();
                $("#carouselConsultoriaIndicators").toggle("fast");
            });
        });
    </script>

</body>
</html>
