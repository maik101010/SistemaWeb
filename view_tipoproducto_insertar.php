<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">
     <style>
       form select {
          width: 100%;
          padding: 10px;
          border-radius: 5px;
          border: 2px solid #ccc;
          color: #757575;
        }
     </style>

      <?php include  "cosas-generales/links-generales.php"; ?>
     <style>
       .btn-regresar {
          display: block;
          margin: 20px auto;
          width: 10%;
        }

        @media screen and (max-width: 1000px) {
          .btn-regresar {
            width: 50%;
          }
        }
     </style>


  <title>Tema</title>
</head>
<body>
     <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
        }else{
         header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Modificar Tipo de Producto</h1>
    
	<form action="php/tipo_producto/insertar_tipo_producto.php" method="post" class="frm-registrarse" id="frm-registrarse">
				
			        <input type="text" class="campo-frm-registrarse" placeholder="Tipo de producto" name="tipo_producto">
			        <select name="tipo_uso">
		                  <option value="0">Elige un tipo de uso</option>
		                  <?php
		                          include "php/conexion.php";
		                          $conexion = $con;
		                          $consulta = $conexion->query("SELECT * FROM tipo_uso");

		                                foreach ($consulta as $row) {
		                                         $opciones='<option value="'.$row["id"].'">'.$row["descripcion"].'</option>';
		                                         echo $opciones;
		                                }              


		                      ?>
                    </select>
			        


			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">
			        <input type="reset" class="btn-borrar" value="Borrar">
    			</form>

         <a href="view_tipoproducto.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>