<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

      <?php include  "cosas-generales/links-generales.php"; ?>
      <style>
        .btn-regresar {
  display: block;
  margin: 20px auto;
  width: 10%;
}

@media screen and (max-width: 1000px) {
  .btn-regresar {
    width: 50%;
  }
}
      </style>

  <title>Tema</title>
</head>
<body>
     <?php
      session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
           $id_usuario = $_SESSION["usuario"][3];
        }else{
         header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Modificar Tipo de uso del producto</h1>
    
    <?php 

    	include "php/conexion.php";
    	$conexion = $con;
        $id = $_GET["id"];
        $consulta = $conexion->query("SELECT * FROM tipo_uso WHERE tipo_uso.id = '$id'");

        	// ---- inicio for
			foreach ($consulta as $row) {	?>
				<form action="php/tipo_uso/modificar_tipo_uso.php" method="post" class="frm-registrarse" id="frm-registrarse">
					<input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>"><input type="hidden" class="campo-frm-registrarse" name="id_usuario" value="<?php echo $id_usuario; ?>">
         
			        <input type="text" class="campo-frm-registrarse" placeholder="descripcion" name="descripcion" value="<?php echo $row['descripcion']; ?>">
			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">
			        <input type="reset" class="btn-borrar" value="Borrar">
    			</form>

			<!-- /// fin for -->
			<?php } ?>

          <a href="view_tipouso_productos.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
   
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>