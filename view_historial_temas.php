<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php include "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_gestionar_tema_estilos.css">

  <title>Historial</title>
</head>
<body>
    <?php
       session_start();
       if (!isset($_SESSION["usuario"])) {
        /*echo "entramos a if";
            header("Location : index.php");*/
            echo "<script>window.location.replace('index.php');
        </script>";
           
        }else if ($_SESSION["usuario"][0]=="Administrador" || $_SESSION["usuario"][0]=="Consultor") {
           include "cosas-generales/header_usuario.php";
          
        }
    ?>

    <h1 class="titulo-principal">Historial Temas</h1>
    
    <div class="contenedor-botones-gestionar-usuarios">
        <a href="consultores.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
        
    </div>
             
    
    <?php 
        include "php/conexion.php";

        $conexion = $con;

        $consulta = $conexion->query("SELECT tipo_usuario.tipo, usuario.nombres, tema.estado, tema.horario, tema.descripcion, tema.fecha_inicio, tema.fecha_fin, tema.costo
			FROM usuario 
			INNER JOIN tipo_usuario ON usuario.tipo = tipo_usuario.id
			INNER JOIN tema ON tema.usuario_id = usuario.id");
        ?>
        <!-- Empieza la tabla             -->
        <div class="table-responsive table-hover container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Consultor</th>
                        <th scope="col">Horario</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Fecha Inicio</th>
                        <th scope="col">Fecha Fin</th>
                        <th scope="col">Costo</th>
                        <th scope="col">Estado Actual</th>
                        <th scope="col">Tipo De Usuario</th>
                        
                    </tr>
                </thead>
            <?php 
            foreach ($consulta as $row) { ?>
            
                    <tbody>
                        <tr>
                            <td><?php echo $row['nombres'] ?></td>
                            <td><?php echo $row['horario'] ?></td>
                            <td><?php echo $row['descripcion'] ?></td>
                            <td><?php echo $row['fecha_inicio'] ?></td>
                            <td><?php echo $row['fecha_fin'] ?></td>
                            <td><?php echo $row['costo'] ?></td>
                            
                            <!-- <td><?php //echo $usuario_modificacion ?></td> -->

                            <?php if ($row['estado']=='1') {
                                echo "<td>Activo</td>";
                            } else
                                echo "<td>Inactivo</td>";
                            ?>   
                            <td><?php echo $row['tipo'] ?></td>                         
                        </tr>
                    </tbody>
                    
            <?php  } ?>
            <!-- Termina la tabla -->
            </table> 
        </div>
    
    

    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    <script language="JavaScript">
       function confirm_delete() {
        return confirm('¿Esta usted seguro?');
        }
    </script>
  
</body>
</html>