<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">
     <style>
       form select {
          width: 100%;
          padding: 10px;
          border-radius: 5px;
          border: 2px solid #ccc;
          color: #757575;
        }
     </style>

      <?php include  "cosas-generales/links-generales.php"; ?>
      <link rel="stylesheet" href="css/view_uinsertar_umodificar_usuario.css">

  <title>Tema</title>
</head>
<body>
     <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
        }else{
         header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Insertar Producto</h1>


  			<form action="php/producto/insertar_producto.php" method="post" class="frm-registrarse" id="frm-registrarse">
					
			        <input type="text" class="campo-frm-registrarse" placeholder="Nombre:" name="nombre" >
			        <input type="number" class="campo-frm-registrarse" placeholder="Precio:" name="precio" min="1">
			        <input type="text" class="campo-frm-registrarse" placeholder="Unidad de medida:" name="unidad_medida">
			        
			        <?php
                         
                        include "php/conexion.php";
    					$conexion = $con;
                        $consulta = $conexion->query("SELECT * FROM tipo_producto");
							echo '<select name="tipo_producto">';
				            foreach ($consulta as $row2) {

				            		$sel = '';
				            		if($row2['id'] == $row['tipoproducto_id']){
				            			$sel = 'selected';
				            		}
				 
				      				echo $opciones='<option value="'.$row2["id"].'"  '.$sel.'>'.$row2["tipo_producto"].'</option>';
						
				                                                     
				            }              
                                    echo '</select>';	
                      ?>
			        
			        <textarea name="descripcion" placeholder="Descripción del producto" form="frm-registrarse"></textarea>



			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Insertar">
			        <input type="reset" class="btn-borrar" value="Borrar">
    			</form>
    			
			
      <a href="view_producto.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
      <!-- <a href="view_consultores.php" class="btn btn-outline-dark btn-instertar-tema">Ver consultores</a> -->
             
   
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>