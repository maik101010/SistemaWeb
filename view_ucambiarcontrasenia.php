<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php 
    /*
    para el manejo de rutas al futuro
    */
      //require_once "php/config.php";

      
     ?>

    <?php include  "cosas-generales/links-generales.php"; ?>
     <link rel="stylesheet" href="css/view_uinsertar_umodificar_usuario.css">

  <title>Tema</title>
</head>
<body>
    <?php
        session_start();
        if (!isset($_SESSION["usuario"])) {
            header("Location: index.php");
        }else{
            include "cosas-generales/header_usuario.php";
        }
    ?>

	<?php 
    	include "php/conexion.php";
    	$conexion = $con;
        $id_usuario = $_GET["id"];
        $consulta = $conexion->query("SELECT usuario.contrasenia, usuario.id FROM usuario WHERE id = '$id_usuario'");
      ?>
        <!-- 	// ---- inicio for -->
    
             
  			


    <h1 class="titulo-principal">Cambiar contraseña</h1>
            <?php foreach ($consulta as $row) {	?>    
                  <form action="php/usuario/modificar_contrasenia.php" method="post" class="frm-registrarse" id="frm-registrarse">
                
                    <input type="text"  disabled class="campo-frm-registrarse" placeholder="actual"  name="actual" value="<?php echo $row['contrasenia']; ?>">
              	    <input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>">
               
                    <input type="password" class="campo-frm-registrarse" placeholder="Contraseña nueva" id="contra" name="contrasenia" >
                    <input type="password" class="campo-frm-registrarse" placeholder="Repetir contraseña" id="contraseniaRepeat"  name="contraseniaRepeat" >

                    <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Actualizar">
                    <input type="reset" class="btn-borrar" value="Borrar">
                </form>

            

	 <a href="view_usuario.php?id=<?php echo $row['id']; ?>" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
	             <?php } ?>
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

</body>
</html>