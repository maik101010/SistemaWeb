<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php 
    /*
    para el manejo de rutas al futuro
    */
      //require_once "php/config.php";

      
     ?>

    <?php include  "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_uinsertar_umodificar_usuario.css">

  <title>Tema</title>
</head>
<body>
   <?php
        session_start();
        if (!isset($_SESSION["usuario"])) {
            header("Location: index.php");
        }else{     
           include "cosas-generales/header_usuario.php";
        }
    ?>

    <h1 class="titulo-principal">Modificar Usuario</h1>
    
    <?php 

    	include "php/conexion.php";
    	  $conexion = $con;
        $id_usuario = $_GET["id"];
        $consulta = $conexion->query("SELECT usuario.id, usuario.email, usuario.contrasenia, usuario.nombres, usuario.a_paterno, usuario.a_materno, tipo_usuario.tipo, usuario.telefono, usuario.celular, usuario.pais, usuario.ciudad
            FROM usuario INNER JOIN tipo_usuario ON usuario.tipo = tipo_usuario.id WHERE usuario.id = '$id_usuario'");
      ?>
        <!-- 	// ---- inicio for -->
      <?php if ($_SESSION["usuario"][0] == "Administrador") {
        $id_usuario_sesion = $_SESSION["usuario"][3];
        ?>
             
  			<?php foreach ($consulta as $row) {	?>
  				<form action="php/usuario/modificar_usuario.php" method="post" class="frm-registrarse" id="frm-registrarse">
  					<input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>">
            <input type="hidden" class="campo-frm-registrarse" name="id_usuario_sesion" value="<?php echo $id_usuario_sesion; ?>">
         
  			        <input type="text" class="campo-frm-registrarse" placeholder="Horario"  name="email" value="<?php echo $row['email']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Nombre"  name="nombres" value="<?php echo $row['nombres']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Apellido paterno"  name="a_paterno" value="<?php echo $row['a_paterno']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Apellido materno"  name="a_materno" value="<?php echo $row['a_materno']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Teléfono"  name="telefono" value="<?php echo $row['telefono']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Celular"  name="celular" value="<?php echo $row['celular']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Pais"  name="pais" value="<?php echo $row['pais']; ?>">
  			        <input type="text" class="campo-frm-registrarse" placeholder="Ciudad"  name="ciudad" value="<?php echo $row['ciudad']; ?>">

  			        <select name="tipo_usuario">
  			        	<option value="0">Tipo de usuario</option>
  			        	<option value="1">Administrador</option>
  			        	<option value="3">Consultor</option>
  			        </select>
  			        
  			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">

  			        <input type="reset" class="btn-borrar" value="Borrar">                
                
      			</form>

  			<!-- /// fin for -->
  			<?php } ?>

        <?php } else { 
            $id_usuario_sesion = $_SESSION["usuario"][3];
          ?>

         <?php foreach ($consulta as $row) { ?>
            <form action="php/usuario/modificar_usuario.php" method="post" class="frm-registrarse" id="frm-registrarse">
              <input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>">

              <input type="hidden" class="campo-frm-registrarse" name="id_usuario_sesion" value="<?php echo $id_usuario_sesion; ?>">
         
                  <input type="text" class="campo-frm-registrarse" placeholder="Horario"  name="email" value="<?php echo $row['email']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Nombre"  name="nombres" value="<?php echo $row['nombres']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Apellido paterno"  name="a_paterno" value="<?php echo $row['a_paterno']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Apellido materno"  name="a_materno" value="<?php echo $row['a_materno']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Teléfono"  name="telefono" value="<?php echo $row['telefono']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Celular"  name="celular" value="<?php echo $row['celular']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Pais"  name="pais" value="<?php echo $row['pais']; ?>">
                  <input type="text" class="campo-frm-registrarse" placeholder="Ciudad"  name="ciudad" value="<?php echo $row['ciudad']; ?>">

                  <select name="tipo_usuario">
                    <option value="0">Tipo de usuario</option>
                    <option value="3" selected>Consultor</option>
                  </select>
                  <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">

                  <input type="reset" class="btn-borrar" value="Borrar">                
                
              </form>
          <!--fin for -->
          <?php } ?>

      <?php } ?>
    
      <a href="view_usuario.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>