<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php 
    /*
    para el manejo de rutas al futuro
    */
      //require_once "php/config.php";

      
     ?>

    <?php include  "cosas-generales/links-generales.php"; ?>
     <link rel="stylesheet" href="css/view_uinsertar_umodificar_usuario.css">

  <title>Tema</title>
</head>
<body>
    <?php
        session_start();
        if (!isset($_SESSION["usuario"])) {
            header("Location: index.php");
        }else{
            include "cosas-generales/header_usuario.php";
        }
    ?>


    <h1 class="titulo-principal">Insertar Usuario</h1>
            <?php 
              if ($_SESSION["usuario"][0] == "Administrador") {?>
                
                  <form action="php/usuario/insertar_usuario.php" method="post" class="frm-registrarse" id="frm-registrarse">
                
                    <input type="text" class="campo-frm-registrarse" placeholder="Email"  name="email" >
                    <input type="password" class="campo-frm-registrarse" placeholder="Contraseña"  name="contrasenia" >

                    <input type="text" class="campo-frm-registrarse" placeholder="Nombre"  name="nombres" >
                    <input type="text" class="campo-frm-registrarse" placeholder="Apellido paterno"  name="a_paterno">
                    
                    <input type="text" class="campo-frm-registrarse" placeholder="Apellido materno"  name="a_materno">
                    
                    <input type="number" class="campo-frm-registrarse" placeholder="Teléfono"  name="telefono">

                    <input type="number" type="number" class="campo-frm-registrarse" placeholder="Celular"  name="celular">
                    <input type="text" class="campo-frm-registrarse" placeholder="Pais"  name="pais">
                    <input type="text" class="campo-frm-registrarse" placeholder="Ciudad"  name="ciudad">

                    <select name="tipo_usuario">
                      <option value="0">Tipo de usuario</option>
                      <option value="1">Administrador</option>
                      <option value="3">Consultor</option>
                    </select>
                    
                    <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Registrar">
                    <input type="reset" class="btn-borrar" value="Borrar">
                </form>

              <?php } else {?>
                  <form action="php/usuario/insertar_usuario.php" method="post" class="frm-registrarse" id="frm-registrarse">
                
                    <input type="text" class="campo-frm-registrarse" placeholder="Email"  name="email" >
                    <input type="password" class="campo-frm-registrarse" placeholder="Contraseña"  name="contrasenia" >

                    <input type="text" class="campo-frm-registrarse" placeholder="Nombre"  name="nombres" >
                    <input type="text" class="campo-frm-registrarse" placeholder="Apellido paterno"  name="a_paterno">
                    
                    <input type="text" class="campo-frm-registrarse" placeholder="Apellido materno"  name="a_materno">
                    
                    <input type="text" class="campo-frm-registrarse" placeholder="Teléfono"  name="telefono">
                    <input type="text" class="campo-frm-registrarse" placeholder="Celular"  name="celular">
                    <input type="text" class="campo-frm-registrarse" placeholder="Pais"  name="pais">
                    <input type="text" class="campo-frm-registrarse" placeholder="Ciudad"  name="ciudad">

                    <select name="tipo_usuario">
                      <option value="0">Tipo de usuario</option>
                      <option value="3" selected>Consultor</option>
                    </select>
                    
                    <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Registrar">
                    <input type="reset" class="btn-borrar" value="Borrar">
                </form>
              
              <?php }?>
               
             

	 <a href="view_usuario.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>