-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para db_global
CREATE DATABASE IF NOT EXISTS `db_global` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_global`;

-- Volcando estructura para tabla db_global.producto
CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` double DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `unidad_medida` varchar(40) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tipoproducto_id` int(11) DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipoproducto_id` (`tipoproducto_id`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`tipoproducto_id`) REFERENCES `tipo_producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla db_global.producto: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` (`id`, `precio`, `nombre`, `unidad_medida`, `imagen`, `tipoproducto_id`, `descripcion`, `id_usuario_modificacion`, `fecha_ult_modificacion`) VALUES
	(3, 790, 'calgonit 6010', 'bidon 20 kg', NULL, 3, 'calgonit R flüssig es un detergente alcalino altamente concentrado. No crea espuma y es fácilmente controlado por conductividad y está recomendado para la limpieza por recirculación y pulverización de depósitos, pasteurizadores, tuberías, calentadore', 17, '2019-01-19'),
	(4, 600, 'Calgonit 6020', 'bidon 30 kg', NULL, 3, 'Adictivo sin cloro', NULL, NULL),
	(5, 460, 'Calgonit 6021', 'bidon 20 kg', NULL, 3, 'Nueva descripción', 17, '2019-01-19'),
	(6, 600, 'Calgonit sterizid forte 15', '1kf', NULL, 4, 'aaa', 17, '2019-01-19'),
	(7, 300, 'Calgonit sporex', '1 kg', NULL, 5, 'Este es un espumante alcalino', NULL, NULL),
	(8, 400, 'Calgonit CF314', '1 lb', NULL, 5, '', NULL, NULL);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;

-- Volcando estructura para tabla db_global.tema
CREATE TABLE IF NOT EXISTS `tema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horario` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  CONSTRAINT `tema_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla db_global.tema: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tema` DISABLE KEYS */;
INSERT INTO `tema` (`id`, `horario`, `direccion`, `fecha_inicio`, `fecha_fin`, `costo`, `usuario_id`, `descripcion`, `fecha_ult_modificacion`, `id_usuario_modificacion`, `estado`) VALUES
	(2, '1pm-4pm', 'Martinez de la torre', '2019-03-01', '2019-02-08', 2000, 10, 'Medio Ambiente', NULL, NULL, '1'),
	(4, '9am-12pm', 'Martinez de la torre', '2019-01-20', '2019-01-27', 400, 9, 'Productos naturales', '2019-01-19', 17, '0'),
	(7, '12pm- 4pm', 'Martinez de la torre', '2019-02-01', '2019-02-08', 900, 9, 'Agricultura', '2019-01-19', 17, '1'),
	(14, '2pm-5pm', 'Martinez de la torre', '2019-02-15', '2019-02-22', 1200, 9, 'Medicion de PH', '2019-01-19', 17, '1'),
	(15, '12pm- 4pm', 'Martinez de la torre', '2019-02-15', '2019-02-22', 1800, 10, 'Agricultura', '2019-01-20', 17, '1'),
	(17, '9am-12pm', 'ninguna', '2019-01-19', '2019-01-20', 690, 18, 'Industria lactea addxa', '2019-01-20', 17, '1');
/*!40000 ALTER TABLE `tema` ENABLE KEYS */;

-- Volcando estructura para tabla db_global.tipo_producto
CREATE TABLE IF NOT EXISTS `tipo_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_producto` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tipouso_id` int(11) DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipouso_id` (`tipouso_id`),
  CONSTRAINT `tipo_producto_ibfk_1` FOREIGN KEY (`tipouso_id`) REFERENCES `tipo_uso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla db_global.tipo_producto: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_producto` DISABLE KEYS */;
INSERT INTO `tipo_producto` (`id`, `tipo_producto`, `tipouso_id`, `id_usuario_modificacion`, `fecha_ult_modificacion`) VALUES
	(3, 'detergentes alcalinos', 1, NULL, NULL),
	(4, 'desinfeccion', 3, 17, '2019-01-19'),
	(5, 'espumantes alcalinos', 2, NULL, NULL),
	(6, 'PRUEBA', 1, NULL, NULL);
/*!40000 ALTER TABLE `tipo_producto` ENABLE KEYS */;

-- Volcando estructura para tabla db_global.tipo_uso
CREATE TABLE IF NOT EXISTS `tipo_uso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla db_global.tipo_uso: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_uso` DISABLE KEYS */;
INSERT INTO `tipo_uso` (`id`, `descripcion`, `id_usuario_modificacion`, `fecha_ult_modificacion`) VALUES
	(1, 'Industria lactea a', 17, '2019-01-19'),
	(2, 'instalaciones mantequeras', NULL, NULL),
	(3, 'Industria cafetera 1', 17, '2019-01-19');
/*!40000 ALTER TABLE `tipo_uso` ENABLE KEYS */;

-- Volcando estructura para tabla db_global.tipo_usuario
CREATE TABLE IF NOT EXISTS `tipo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla db_global.tipo_usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` (`id`, `tipo`) VALUES
	(1, 'Administrador'),
	(3, 'Consultor');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;

-- Volcando estructura para tabla db_global.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `a_paterno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `a_materno` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `contrasenia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `celular` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `pais` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `ciudad` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_usuario_modificacion` int(11) DEFAULT NULL,
  `fecha_ult_modificacion` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `tipo` (`tipo`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla db_global.usuario: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nombres`, `a_paterno`, `a_materno`, `email`, `contrasenia`, `telefono`, `celular`, `pais`, `ciudad`, `tipo`, `estado`, `id_usuario_modificacion`, `fecha_ult_modificacion`) VALUES
	(9, 'Omar a', 'Florez', 'Ramirez', 'prueba@gmail.com', '123', '2310565', '2321279832', 'Mexico', 'Martinez de la Torre', 3, '1', NULL, '2019-01-19'),
	(10, 'Mario Alberto', 'Perez', 'Ramos', 'mario.13yanet@gmail.com', '123', '2323731201', '2321090909', 'Mexico', 'Martinez de la Torre', 1, '1', 17, '2019-01-19'),
	(15, 'Samuelita', 'Arellanes', 'Martinez', 'admin_nuevo@gmail.com', '12345', '321654789', '2321030314', 'Mexico', 'Martinez de la Torre', 1, '0', NULL, '2019-01-19'),
	(17, 'Iram', 'Tenorio', 'Abelló', 'michaedu_96@gmail.com', '123', '56423154', '2321212314', 'Mexico', 'Martinez de la Torre', 1, '1', NULL, NULL),
	(18, 'Norma', 'Galindo', 'Suarez', 'norma@gmail.com', '123', '51236484', '2321030312', 'Mexico', 'Martinez de la Torre', 3, '1', NULL, NULL),
	(19, 'alex', 'lforez', 'gomez', 'alex@gmail.com', '$2y$12$6WGc4ncY9rn3RRCUPK.chuhviqNB55Z.L9vUQ1/ZmeqGOysOkLw7C', '1233', '311222333', 'Colombia', 'cartagena', 1, '1', NULL, NULL),
	(20, 'mario', 'PEREZA', 'orjuela', 'mario.13yanet12345@gmail.com', '$2y$12$Oh9.BLo.coM088bzOeLsyeiYwi1hX8hM5MHcGfDhjyndt9Y7daRC2', '12221', '333222111', 'MEXICO', 'MONTERREY', 1, '1', NULL, NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
