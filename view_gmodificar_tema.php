<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php 
    /*
		para el manejo de rutas al futuro
    */
    	//require_once "php/config.php";

    	
     ?>

    <?php include  "cosas-generales/links-generales.php"; ?>
    <style>
      .btn-regresar {
        display: block;
        margin: 20px auto;
        width: 10%;
      }

      @media screen and (max-width: 1000px) {
        .btn-regresar {
          width: 50%;
        }
      }
    </style>

  <title>Tema</title>
</head>
<body>
    <?php
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador" || $_SESSION["usuario"][0]=="Consultor") {
           include "cosas-generales/header_usuario.php";
           $id_usuario_sesion = $_SESSION["usuario"][3];
        }else{
            header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Modificar Tema</h1>
    
    <?php 

    	include "php/conexion.php";
    	$conexion = $con;
        $id_tema = $_GET["id"];
        $consulta = $conexion->query("SELECT  tema.id, tema.horario, tema.direccion, tema.fecha_inicio, tema.fecha_fin, tema.costo, tema.descripcion,
									CONCAT(usuario.nombres, ' ', usuario.a_paterno) as nombre_per FROM tema INNER JOIN usuario ON 
									usuario.id = tema.usuario_id where tema.id = '$id_tema'");

        	// ---- inicio for
			foreach ($consulta as $row) {	?>
				<form action="php/tema/modificar_tema.php" method="post" class="frm-registrarse" id="frm-registrarse">
					<input type="hidden" class="campo-frm-registrarse" name="id" value="<?php echo $row['id']; ?>">
          <input type="hidden" class="campo-frm-registrarse" name="id_usuario_sesion" value="<?php echo $id_usuario_sesion; ?>">
			        <input type="text" class="campo-frm-registrarse" placeholder="Horario"  name="horario" value="<?php echo $row['horario']; ?>">
			        <input type="text" class="campo-frm-registrarse" placeholder="Dirección"  name="direccion" value="<?php echo $row['direccion']; ?>">
			        <input type="date" class="campo-frm-registrarse" placeholder="Fecha de inicio" name="fecha_inicio" value="<?php echo $row['fecha_inicio']; ?>">
			        <input type="date" class="campo-frm-registrarse" placeholder="Fecha Final" name="fecha_fin" value="<?php echo $row['fecha_fin']; ?>">
			        <input type="number" class="campo-frm-registrarse" placeholder="Costo" name="costo" value="<?php echo $row['costo']; ?>">
			        <input type="text" class="campo-frm-registrarse" name="usuario_id"  readonly="readonly" value="<?php echo $row['nombre_per']; ?>">
			        <input type="text" class="campo-frm-registrarse" placeholder="Descripción" name="descripcion" value="<?php echo $row['descripcion']; ?>">
			        
			        <input type="submit" class="btn-registrarse" id="btn-registrarse" value="Modificar">
			        <input type="reset" class="btn-borrar" value="Borrar">
    			</form>

			<!-- /// fin for -->
			<?php } ?>

        
     <a href="view_gestionar_tema.php" class="btn btn-outline-dark btn-regresar"><i class="fa fa-chevron-left"></i> Regresar</a>
             
   
    <!-- 
 -->
   
    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>
  
</body>
</html>