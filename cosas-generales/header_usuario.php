 <header class="header" id="header">
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
 <nav class="menu">
  <div class="logo">
      <img src="img/logo.jpg" alt="">
    </div>
    
      <div class="contenedor-btn-menu-responsive">
        <a href="#" class="btn-menu" id="btn-menu"><i class="icono fa fa-bars"></i></a>
      </div>

      <div class="enlaces" id="enlaces">
            <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
            <a href="quienes-somos.php"><i class="fa fa-user-circle-o"></i> Acerca</a>
            <a href="proveedor.php"><i class="fa fa-truck"></i> Proveedores</a>
            <a href="consultores.php"><i class="fa fa-street-view"></i> Consultores</a>
            <a href="view_producto_tabla.php"><i class="fa fa-tags"></i> Productos</a>
            <a href="contacto.php"><i class="fa fa-envelope-o"></i> Contacto</a>

            <?php 
              if ($_SESSION["usuario"][0]=="Administrador") {?>
                <a href="view_restaurar_bd.php"><i class="fa fa-database"></i> Backup</a>    
              <?php } ?>
             <!-- Mostramos la sesión del usuario -->
            <a href='view_usuario.php'><i class='fa fa-user' aria-hidden='true'></i><?php echo $_SESSION["usuario"][2]; ?></a>
            
			      <a href="php/cerrar_sesion.php"><i class="fa fa-sign-out"></i> Cerrar sesión</a>

      </div>
    </nav>
</header>
