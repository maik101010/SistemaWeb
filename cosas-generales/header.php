 <header class="header" id="header">
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
 <nav class="menu">
    <div class="logo">
      <img src="img/logo.jpg" alt="">
    </div>

      <div class="contenedor-btn-menu-responsive">
        <a href="#" class="btn-menu" id="btn-menu"><i class="icono fa fa-bars"></i></a>
      </div>

      <div class="enlaces" id="enlaces">
            
            <a href="index.php"><i class="fa fa-home"></i> Inicio</a>
            <a href="quienes-somos.php"><i class="fa fa-user-circle-o"></i> Acerca</a>
            <a href="proveedor.php"><i class="fa fa-truck"></i> Proveedores</a>
            <a href="consultores.php"><i class="fa fa-street-view"></i> Consultores</a>
            <a href="view_producto_tabla.php"><i class="fa fa-tags"></i> Productos</a>
            <a href="contacto.php"><i class="fa fa-envelope-o"></i> Contacto</a>
            <a href="login.php"><i class="fa fa-sign-in"></i> Iniciar Sesión</a>
      </div>
    </nav>
</header>