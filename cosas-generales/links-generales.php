<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="img/logo.jpg">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/estilos-generales.css">
<link rel="stylesheet" href="css/menu.css">
<link rel="stylesheet" href="css/banner.css">
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/modal.css">
<link rel="stylesheet" href="css/frm-iniciar-sesion.css">
<link rel="stylesheet" href="css/frm-registrarse.css">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="icomoon.css">