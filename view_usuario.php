<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">

    <!-- Estilos -->
    <?php include "cosas-generales/links-generales.php"; ?>
    <link rel="stylesheet" href="css/view_gestionar_tema_estilos.css">

  <title>Usuarios</title>
</head>
<body>
   
    <?php 
      
       session_start();
        if ($_SESSION["usuario"][0]=="Administrador") {
           include "cosas-generales/header_usuario.php";
        }else if ($_SESSION["usuario"][0]=="Consultor") {
            /**
             * Si la sesión es de tipo consultor, redireccionamos a la vista de los consultores
             */
            header("location: view_consultor.php");
        }else{
            header("Location: index.php");
        }
    ?>

    <h1 class="titulo-principal">Gestionar Usuarios</h1>

    <div class="contenedor-botones-gestionar-usuarios">
        <a href="view_uinsertar_usuario.php" class="btn btn-outline-dark">Insertar Usuario</a>
        <a href="view_tipouso_productos.php" class="btn btn-outline-dark">Gestionar Tipo Uso Productos</a>
        <a href="view_tipoproducto.php" class="btn btn-outline-dark">Gestionar Tipo De Productos</a>        
        <a href="view_producto.php" class="btn btn-outline-dark">Gestionar Producto</a>    
        <?php 
        include "php/conexion.php";

        $conexion = $con;
        $sesion = $_SESSION["usuario"];

        /**
         * 
         * Capturamos el id del usuario donde el email sea igual a al valor traido en la sesión
         * 
         */
        $consulta = $conexion->query("SELECT usuario.id FROM usuario WHERE email='$sesion[1]'");

        foreach ($consulta as $row) {
            ?>      
            <a href="view_ucambiarcontrasenia.php?id=<?php echo $row['id']; ?>" class="btn btn-outline-dark">Cambiar contraseña</a>       
        <?php } ?>
    </div>
    
    <?php 
        include "php/conexion.php";

        $conexion = $con;

        /*
            Consulta que trae los usuarios de tipo "consultor", puesto que son los unicos que tienen un tema asociado
        */
        
        $consulta = $conexion->query("SELECT usuario.fecha_ult_modificacion, usuario.estado, usuario.id, usuario.email, usuario.contrasenia, concat (usuario.nombres, ' ',  usuario.a_paterno, ' ', usuario.a_materno) AS nombre, tipo_usuario.tipo, usuario.telefono, usuario.celular, usuario.pais, usuario.ciudad
            FROM usuario INNER JOIN tipo_usuario ON usuario.tipo = tipo_usuario.id");

        ?>
        <!-- Empieza la tabla             -->
        <div class="table-responsive container">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">Email</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Tipo de usuario</th>
                        <th scope="col">Teléfono Celular</th>
                        <th scope="col">Ciudad</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Última modificación</th>
                        <!-- <th scope="col">Modificado por</th> -->
                        <th scope="col">Operaciones</th>
                    </tr>
                </thead>
                <tbody>
            <?php 
                    foreach ($consulta as $row) { ?>
                        <tr>
                            <td><?php echo $row['email'] ?></td>
                            <td><?php echo $row['nombre'] ?></td>
                            <td><?php echo $row['tipo'] ?></td>
                            <td><?php echo $row['celular'] ?></td>
                            <td><?php echo $row['ciudad'] ?></td>
                          
                            <?php if ($row['estado']=='1') {
                                echo "<td>Activo</td>";
                            } else
                                echo "<td>Inactivo</td>";
                            ?>
                            <td><?php echo $row['fecha_ult_modificacion'] ?></td>
                            <!-- <td><?php// echo $row['nombres'] ?></td>  -->

                            <td class="contenedor-btn-editar-eliminar">
                                <?php if ($row['estado']=='1') {?>
                                   <a href="view_umodificar_usuario.php?id=<?php echo $row['id'] ?>" class="btn-editar"><i class="fa fa-edit"></i></a>
                                    <a href="php/usuario/eliminar_usuario.php?id=<?php echo $row['id']?>" onclick="return confirm_delete()" class="btn-eliminar"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                                
                            </td>
                        </tr>
            <?php  } ?>
                </tbody>
            <!-- Termina la tabla -->
            </table> 
        </div>
    
    

    <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    <script language="JavaScript">
       function confirm_delete() {
        return confirm('¿Esta usted seguro?');
        }
    </script>
  
</body>
</html>