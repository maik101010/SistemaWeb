<?php 

	include "../conexion.php";

	$conexion = $con;

	$horario = $_POST["horario"];
	$direccion = $_POST["direccion"];

	$fecha_inicio = $_POST["fecha_inicio"];
	$fecha_actual = date('Y-m-d');
	
	$fecha_fin = $_POST["fecha_fin"];

	if ($fecha_inicio<$fecha_actual) {
		echo "<script>alert('Un momento, la fecha de inicio debe ser actual')</script>";
		header("Location: ../../view_ginsertar_tema.php");
	
	}else if ($fecha_fin< $fecha_inicio) {
		echo "<script>alert('La fecha final debe ser mayor que la fecha de inicio')</script>";
		header("Location: ../../view_ginsertar_tema.php");
	}

	else{

		$costo = $_POST["costo"];
		$descripcion = $_POST["descripcion"];
		$id_usuario = $_POST["usuario_id"];
		$estado = 1;
		$sentencia = $conexion->prepare("INSERT INTO tema (horario, direccion, fecha_inicio, fecha_fin, costo, descripcion, usuario_id, estado) VALUES (?,?,?,?,?,?,?, ?)");
		$sentencia->bindParam(1, $horario);
		$sentencia->bindParam(2, $direccion);
		$sentencia->bindParam(3, $fecha_inicio);
		$sentencia->bindParam(4, $fecha_fin);
		$sentencia->bindParam(5, $costo);
		$sentencia->bindParam(6, $descripcion);
		$sentencia->bindParam(7, $id_usuario);

		$sentencia->bindParam(8, $estado);
		
		if ($sentencia->execute()) {
				//header("Location: ../../view_gestionar_tema.php");
				echo "<script>alert('Registro insertado')
					window.location.replace('../../consultores.php');
					</script>";
				//echo "<script>alert('Insertado')</script>";
			
		}else{
				echo "<script>alert('Upps, error')</script>";
				header("Location: index.php");
			}

		}
	

?>
