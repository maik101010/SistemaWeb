<?php 

	include "../conexion.php";

	$conexion = $con;


	$id = $_POST["id"];

	$horario = $_POST["horario"];
	$direccion = $_POST["direccion"];
	$fecha_inicio = $_POST["fecha_inicio"];
	$fecha_fin = $_POST["fecha_fin"];
	$costo = $_POST["costo"];
	$descripcion = $_POST["descripcion"];
	$id_usuario_modificacion = $_POST["id_usuario_sesion"];
	$fecha = date("Y-m-d");
	

	$sentencia = $conexion->prepare("UPDATE tema set horario = ?, direccion = ?, fecha_inicio = ?, fecha_fin = ?, costo = ?, descripcion = ?, id_usuario_modificacion=?, fecha_ult_modificacion =? WHERE id = '$id'");
	$sentencia->bindParam(1, $horario);
	$sentencia->bindParam(2, $direccion);
	$sentencia->bindParam(3, $fecha_inicio);
	$sentencia->bindParam(4, $fecha_fin);
	$sentencia->bindParam(5, $costo);
	$sentencia->bindParam(6, $descripcion);
	$sentencia->bindParam(7, $id_usuario_modificacion);
	$sentencia->bindParam(8, $fecha);
	
	if ($sentencia->execute()) {
			echo "<script>alert('Registro actualizado')
					window.location.replace('../../view_gestionar_tema.php');
				</script>";
		
	}else{
		echo "<script>alert('Ocurrio un error')
					window.location.replace('../../view_gestionar_tema.php');
				</script>";		
			
		}
	

?>