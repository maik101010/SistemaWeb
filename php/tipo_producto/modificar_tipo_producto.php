<?php 

	include "../conexion.php";

	$conexion = $con;


	$id = $_POST["id"];

	$tipo_producto = $_POST["tipo_producto"];
	$tipo_uso = $_POST["tipo_uso"];
	$id_usuario_modificacion = $_POST["id_usuario"];
	$fecha = date("Y-m-d");
	

	$sentencia = $conexion->prepare("UPDATE tipo_producto SET tipo_producto = ?, tipouso_id = ?, id_usuario_modificacion = ?, fecha_ult_modificacion = ? WHERE id = '$id'");
	$sentencia->bindParam(1, $tipo_producto);
	$sentencia->bindParam(2, $tipo_uso);
	$sentencia->bindParam(3, $id_usuario_modificacion);
	$sentencia->bindParam(4, $fecha);
	
	if ($sentencia->execute()) {
			echo "Actualizado";
			header("Location: ../../view_tipoproducto.php");
		
	}else{
			echo "Ocurrio un error";
			header("Location: index.php");
		}
	

?>