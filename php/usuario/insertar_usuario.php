<?php 

    include "../conexion.php";

    $conexion = $con;

    if (isset($_POST["tipo_usuario"]) && !empty($_POST["tipo_usuario"]) && isset($_POST["email"]) && !empty($_POST["email"]) && isset($_POST["contrasenia"]) &&!empty($_POST["contrasenia"])) {
            $email = $_POST["email"];
            $contrasenia = $_POST["contrasenia"];


            $contrasenia_encriptada = password_hash($contrasenia, PASSWORD_DEFAULT, array('cost' => 12));
            
            $nombres = $_POST["nombres"];
            $a_paterno = $_POST["a_paterno"];
            $a_materno = $_POST["a_materno"];
            $telefono = $_POST["telefono"];
            $celular = $_POST["celular"];
            $pais = $_POST["pais"];
            $ciudad = $_POST["ciudad"];
            $tipo_usuario = $_POST["tipo_usuario"];
            $estado_usuario = 1;

            /*
                Consultamos el total de valores que hay acorde al correo digitado
            */
            $validarEmail = "select count(*) as total from usuario where email = ?";
            $result = $conexion->prepare($validarEmail);
            $result->bindParam(1, $email, PDO::PARAM_STR);
            $result->execute();
            /*
                Validamos que el correo no exista
            */
            if ($result->fetchColumn() == 0) {
               
                $sentencia = $conexion->prepare("INSERT INTO usuario (email, contrasenia, nombres, a_paterno, a_materno, telefono, celular, pais, ciudad, tipo, estado) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                $sentencia->bindParam(1, $email);
                $sentencia->bindParam(2, $contrasenia_encriptada);
                $sentencia->bindParam(3, $nombres);
                $sentencia->bindParam(4, $a_paterno);
                $sentencia->bindParam(5, $a_materno);
                $sentencia->bindParam(6, $telefono);
                $sentencia->bindParam(7, $celular);
                $sentencia->bindParam(8, $pais);
                $sentencia->bindParam(9, $ciudad);
                $sentencia->bindParam(10, $tipo_usuario);
                $sentencia->bindParam(11, $estado_usuario);
                
            
                if ($sentencia->execute()) {
                        echo "Insertado";
                        header("Location: ../../view_usuario.php");
                    
                }else{
                        echo "Ocurrio un error";
                        header("Location: index.php");
                    }

            }else{
                echo "<script>alert('El correo ya existe')</script>";
                header("Location: ../../view_uinsertar_usuario.php");
            }

    }else{
        echo "<script>alert('Error)</script>";
    }




   
    

?>