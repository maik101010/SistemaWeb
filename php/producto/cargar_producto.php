<?php 
  include "../conexion.php";

  $conexion = $con;
  $id_tipo = $_POST["id_tipo_producto"];
  $consulta = $conexion->query("SELECT * FROM producto WHERE tipoproducto_id = '$id_tipo'");
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
    <div class="row contenedor-tabla-busqueda">
      <div class="contenedor-busqueda col-md-4">
       <input type="text" id="buscar" placeholder="Buscar Producto">
       <div id="resultado"></div>
      </div>

      <div class="table-responsive col-md-8">
        <table class="table table-hover">
              <thead>
                  <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Unidad medida</th>
                      <th scope="col">Precio</th>
                      <!-- <th scope="col">Operaciones</th> -->
                  </tr>
              </thead>
              <h1>Información del Producto</h1>
          <?php 

          foreach ($consulta as $row) { ?>
          
              <tbody>
                  <tr>
                      <!-- <td><?php //echo $row['id'] ?></td> -->
                      <td><?php echo $row['nombre'] ?></td><!-- 
                      <td><?php // echo $row['descripcion'] ?></td> -->
                      <td><?php echo $row['unidad_medida'] ?></td>
                      <td><p>$<?php echo $row['precio'] ?></p></td>
                     
                      <!-- <td> -->
                         <!-- <a href="mostrar_pdf.php?id=<?php //echo $row['id'] ?>" target="_blank"><i class="fa fa-eye"></i></a>-->
                          
                      <!-- </td> -->
                  </tr>
              </tbody>
                  
               <?php  } ?>
          </table>
      </div>      
    </div>
    

    <script type="text/javascript">
      $(document).ready(function(e){
          $("#buscar").keyup(function(e){

            var nombre = $("#buscar").val();
            /*
              Capturamos el id del tipo de producto
            */
            var id = <?php echo $id_tipo ?>;
            $.ajax({
              type: "post",
              url: "php/producto/buscar_producto.php",
              dataType:"html",
              /*
              Enviamos el id del tipo de producto, junto al nombre digitado en el campo de texto
              */  
              data:{nombre : nombre, id : id}, 
              success: function(dato){
                $("#resultado").empty();
                $("#resultado").append(dato);

              }
            });
          });

      });

    </script>
    

