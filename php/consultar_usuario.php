<?php 
	
include "conexion.php";

$conexion = $con;
	if (isset($_POST["email"]) && !empty($_POST["email"]) && isset($_POST["contrasenia"]) && !empty($_POST["contrasenia"])) {


		$email = $_POST["email"];
		$contrasenia = $_POST["contrasenia"];

		$validarEmail = "SELECT usuario.id, usuario.nombres, usuario.estado, tipo_usuario.tipo, usuario.contrasenia, usuario.email FROM usuario INNER JOIN tipo_usuario on tipo_usuario.id = usuario.tipo WHERE usuario.email = :email and usuario.estado = 1";

		$result = $conexion->prepare($validarEmail);
		$result->bindValue(":email", $email);
		//$result->bindValue(":contrasenia", $contrasenia);
		$result->execute();
		
		//---Capturamos todos los resultados de la base de datos provenientes de la anterior consulta
		$resultados = $result->fetchAll(PDO::FETCH_ASSOC);
		$cont=0;
		foreach ($resultados as $row) {
			//por cada resultado, verificamos si la contrasenia es igual a la que tiene el hash
			if (password_verify($contrasenia, $row["contrasenia"])) {
				$cont++;
				$usuario = $row["tipo"];
				$pass = $row["contrasenia"];
				$emailBd = $row["email"];
				$nombre = $row["nombres"];
				$id_usuario = $row["id"];
			}		
		
			
			
		}

		//Si contador es mayor que uno y por tanto tenemos usuario con hash igual que la contrasenia
		if ($usuario=="Administrador" and $con>0) {
			
						session_start();	
						/**
						 * Enviamos el email, el nombre y id del usuario actual
						 */
						$_SESSION["usuario"] = array("Administrador", $emailBd, $nombre, $id_usuario);

						/*
							Redireccionamos al index con la sesión abierta
						*/
						header("location:../index.php");
		}
			elseif ($usuario=="Consultor" and $con>0){
						session_start();
						$_SESSION["usuario"] = array("Consultor", $emailBd, $nombre, $id_usuario);
						/*
							Redireccionamos al index con la sesión abierta
						*/
						header("location:../index.php");
						
			}else{
						echo "<script>alert('Usuario o contraseña incorrectos')</script>";
						header("location:../login.php");
						
				}
	}else{
		echo "Faltan algunos datos";
	}
	

 ?>