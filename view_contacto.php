<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Fuentes De Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Calligraffitti|Open+Sans|Oswald|Roboto|Shadows+Into+Light+Two|Nunito+Sans" rel="stylesheet">
  <script type="text/javascript">
  function validarSiNumero(numero){
    if (!/^([0-9])*$/.test(numero))
      alert("El valor " + numero + " no es un número");
  }

  </script>

	 <!-- Estilos -->
   <?php include "cosas-generales/links-generales.php"; ?>
   <link rel="stylesheet" href="css/contacto_estilos.css">
    <style>
      body {
        background: #E1F8F2;
      }
    </style>

	<title>Contacto</title>


	 <?php 
      /*
        reanudamos sesión almacenada, sino se ha iniciado la sesión mostramos el header, de lo contrario mostramos el header con lo correspondiente al usuario

      */
        session_start();

        if (!isset($_SESSION["usuario"])) {
            include "cosas-generales/header.php";
        }else{
            include "cosas-generales/header_usuario.php";
        }
     ?>

   <h1 class="titulo-principal">Formulario de contacto</h1>
  
    <form action="php/enviar_mail.php" method="post" class="frm-iniciar-sesion" id="frm-registrarse">
        <input type="email" class="campo-frm-iniciar-sesion" placeholder="Email: " name="email">
        <input type="text" class="campo-frm-iniciar-sesion" placeholder="Nombre:" name="nombre">
        <!-- <input type="text" class="campo-frm-iniciar-sesion" placeholder="Telefono" name="telefono" size="10" onChange="validarSiNumero(this.value);"> -->
        <input type="number" class="campo-frm-iniciar-sesion" placeholder="Telefono" name="telefono" min="1" onpaste="return false"> 
        <input type="text" class="campo-frm-iniciar-sesion" placeholder="Asunto" name="asunto">

 		<textarea rows="4" cols="50" name="mensaje" form="frm-registrarse">Mensaje:</textarea>

        <input type="submit" class="btn-iniciar-sesion" id="btn-entrar-iniciar-sesion" value="Enviar">        
    </form>
      <?php include "cosas-generales/footer.php"; ?>

    <?php include "cosas-generales/scripts-generales.php"; ?>

    </head>
<body>
	
</html>